package main

import (
	"fmt"
	"github.com/rohanthewiz/quick_template_exercises/templates"
	"bytes"
)

func main() {
	fmt.Printf("%s\n", templates.Hello("Foo"))
	fmt.Printf("%s\n", templates.Hello("Bar"))

	names := []string{"Kate", "Go", "John", "Brad"}

	// qtc creates Write* functions for each template function.
	// Such functions accept an io.Writer as their first parameter.
	var buf bytes.Buffer
	templates.WriteGreetings(&buf, names)
	fmt.Printf("buf = \n%s", buf.Bytes())
}
